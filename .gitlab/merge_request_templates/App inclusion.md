* [ ] The app complies with the [inclusion criteria](https://f-droid.org/docs/Inclusion_Policy)
* [ ] The original app author has been notified (and supports the inclusion)
* [ ] All related fdroiddata and rfp issues have been referenced in this merge request
* [ ] The app repo contains the app metadata (summary/description/images/changelog/etc) in a [Fastlane](https://gitlab.com/snippets/1895688) or [Triple-T](https://gitlab.com/snippets/1901490) folder structure
* [ ] Builds with `fdroid build`

---------------------

